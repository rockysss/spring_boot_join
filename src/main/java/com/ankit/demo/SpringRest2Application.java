package com.ankit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class SpringRest2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringRest2Application.class, args);
		System.out.println("ndks");
	}

}
